<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:14:09 GMT -->
<head>
    <title>Drprocare | For Heart</title>     

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

     <?php include 'head.php'; ?>
</head> 

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

  

    <!--=== Team v7 ===-->
    <div class="container-fluid" style="margin-top: 10px;">
        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">For Heart</span>
                        <span class="team-v7-name">Tadasana (Palm Tree Pose):</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Tadasana is simply known for stretching, which forms the basis for all standing as standing asanas. It performed at the beginning and end of the Surya Namaskar sequence and is a key asana for all yoga practices. It’s a good asana for cultivating strength, sense of relaxed power and it also can be helpful for both physically and mentally grounding, and can be used to form a connection with the earth.</p>

                        <p>Steps: 1. Stand straight with an erect spine. 2. Legs should be slightly apart and hands at your sides. 3. Now take a deep breath and raise your both the hands up.4. Interlock your fingers and raise your heels up to stand on your toes. 5. Hold this position for few seconds and keep breathing deeply. 6. Feel the stretch in your legs, arms, and chest. 7. Now while exhaling bring down your hands and heels to come to the starting position. 8. Repeat this for 10 times.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_heart/heart-1.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Trikonasana (Triangular Pose):</span>                        
                        <!-- <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p>This is an easier variation of the triangular pose. It is a heart opening yoga asana in which standing posture designed to promote cardiovascular exercises. The chest expands while breathing and becomes deep & rhythmical. It helps to increase stamina also. The increased rate that one experiences while practicing the posture is one reason why it is generally held for a shorter period of time which, teaches the body how to ramp up heart rate quickly as well as lower heart rate quickly. Also great for stamina building.</p>

                        <p>Steps: 1. Stand erect. Now, keep distance between your legs about 3 to 4 feet. 2. Extend your arms at the shoulder level. 3. Inhale and raises your right arm by the side of your head. 4. Now, bend your right arms with exhaling towards the left side by keeping your body weight equally on both the feet. 5. You should ensure that the right arm become parallel to the ground. 6. Maintain the position as per your comfort with normal breathing and come to the original position by inhaling. 7. Do the same procedure with the left arm. 8. Perform three to five rounds of trikonasana.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_heart/heart-2.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Padangusthasana (Big-Toe Pose):</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>This is a very easy yoga asana to relieve the stress level and helps to get relax. It gives a brilliant stretch in the hamstrings and calves. It is a great asana for building strength in your lower body, calming your senses, gives you focus and has been recommended to patients of depression too.</p>

                        <p>Steps: 1. To begin this asana, you must stand up straight and place your feet parallel to each other. Your feet must be at least six inches apart, and your legs should be straight. 2. Contract your thigh muscles so that your kneecaps will be lifted outwards. 3. Now bend forward, aiming to touch your forehead to your knees. You must ensure that your head and torso move together. 4. Hold your big toe with the fingers of the respective side. The grip must be firm. 5. Inhale and lift the torso. Remember to straighten your elbows. 6. Exhale and bend towards your toes again. Do this a few times. Then, straighten your body. Don’t forget to keep the breath constant and your torso straight as you go up and down. Keep holding your toes throughout. 7. Come back to position.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_heart/heart-3.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Virabhadrasana (The warrior pose):</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>This yoga asana pose helps to improve balance in the body and increased stamina. It also improves blood circulation and releases stress. This is a wonder asana for so many reasons. It makes your strong in the body and in the mind, you learn to open and challenge each muscle of your body. Practicing this asana for over six months keeps the heart rate in check.</p>

                        <p>Steps: 1. Stand straight with your legs hip-distance apart and arms on your sides.2. Now, spread your legs wide-apart.3. Turn your left feet out to face the left side of the mat.4. The other feet will turn inward to make a 45 Degree angle. 5. Twist your body to left completely.6. Go forward on your left knee making a 90 Degree angle; make sure that your knee doesn't cross over the toe. 7. Let the other leg stretch properly.8. Now go into a Namaste position, and taking our arms up above your head, look up, arch your back a bit and stretch.9. Hold for 10 seconds and repeat on the other side.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_heart/heart-4.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        
    </div>
    <!--=== End Team v7 ===-->

    

    <!--=== Footer Version 1 ===-->
   <?php include'footer.php';  ?>
    <!--=== End Footer Version 1 ===-->
</div>




</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:15:23 GMT -->
</html>