<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:14:09 GMT -->
<head>
    <title>Drprocare | For Ankles</title>    

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

     <?php include 'head.php'; ?>
</head> 

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

  

    <!--=== Team v7 ===-->
    <div class="container-fluid" style="margin-top: 10px;">
        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">FOR Ankles</span>

                        <span class="team-v7-name">Downward-Facing Dog Pose (Adho Mukha Svanasana)</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Steps for ankles</p>
                         <p>1. Start in Downward Dog to slowly stretch the legs, calves, and the back of the thighs.</p>
                         <p> 2. Keep your knees bent and slowly straighten one leg at a time, bringing one heel towards the ground.</p>
                         <p> 3. Be gentle and give your legs the needed attention without demanding too much from them.</p>
                         <p> 4. Stay for 1 to 2 minutes.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_ankles/ankle-1.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Legs-Up-the-Wall Pose (Viparita Karani)</span>                        
                        <!-- <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p>This pose is one of the best for relaxing tired legs and feet. Since it's a mild inversion, it will drain lymphatic fluids from the legs which reduces swelling in the feet, ankles, and knees. At the same time, this pose increases the circulation in the upper body and brings back the balance after spending a long time on your feet.
                            <p>1. Lie on the floor and bring your sitting bones close to the wall. </p>
                            <p>2. With any discomfort on the lower back, bring your sitting bones a bit further from the wall.</p>
                            <p> 3. Make sure your kneecaps are not locking and rest your arms on your side. </p>
                            <p>4. Stay anywhere from 3 to 5 minutes and work your way up to 10 minutes.</p>

                        
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_ankles/ankle-2.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Wide-Angle Bend on the Wall Pose (Upavistha Konasana Variation)</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Steps: </p>
                        <p>1. From Legs-Up-the-Wall, keep your legs straight and your sitting bones touching the wall.</p>
                        <p> 2. Start opening your legs into a v-shape until you reach a point of comfortable resistance.</p>
                        <p> 3. Don't pull or push your legs down by force, but let the gravity do the work. </p>
                        <p>4. This pose opens the hips and stretches the inner thighs.</p>
                        <p> 5. Stay for 3 to 5 minutes, then slowly bring your legs back up.</p>

                    
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_ankles/ankle-3.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Eye-of-the-Needle Pose against the Wall (Sucirandhrasana Variation)</span>                        
                       <!--  <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p>This pose relieves stiffness in the outer hips and increases the range of motion in the lower body.It might be easier to begin with your sitting bones a bit further from the wall for this pose.</p>

                        <p>Steps:</p>
                        <p> 1. Start with both legs straight up against the wall, and bring the left ankle above the right knee (above in this case being close to the thigh).</p>
                        <p> 2. Slowly start to bend the right leg, and let your foot slide down against the wall. </p>
                        <p>3. Observe how your hip feels, and slowly progress lower if your body allows.</p>
                        <p> 4. Stop when your ankle is parallel to your knee.</p>
                        <p> 5. Stay for 3 to 5 minutes, and when coming out, keep both legs up for a few breaths before switching sides.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_ankles/ankle-4.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Supported Butterfly Pose (Baddha Konasana)</span>
                        <!-- <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Butterfly helps open the hips and thighs, releases stress and anxiety, and stimulates the digestive organs.</p>

                        <p>Steps:</p>
                        <p> 1. To move into the supported version of Butterfly, bring the soles of your feet together and slide the feet slowly down towards your hips.</p>
                        <p> 2. Take the knees as wide as they go, again, allowing gravity to help you.</p>
                        <p> 3. Stay in the pose for 3 to 5 minutes, after which you can slowly release, bringing your knees close to your chest to move to the final pose.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_ankles/ankle-5.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Happy Baby Pose (Ananda Balasana)</span>                        
                        <!-- <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p>Steps:</p>
                        <p> 1. Happy Baby Pose relieves stress, gives a mild stretch on the inner groin, and relieves pain in the lower back.</p>
                        <p> 2. From Supported Butterfly, bring your knees close to your chest and take a hold of the outsides of your feet with your hands.</p>
                        <p> 3. Lift the feet up, keeping the ankles above the knees.</p>   <p>4. If your hips feel tight, you can place a belt on the ball of each foot, and hold onto the belts for more space.</p>
                        <p> 5. Stay up to 1 minute, after which you can slowly release the legs.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_ankles/ankle-6.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

       
    </div>
    <!--=== End Team v7 ===-->

    

    <!--=== Footer Version 1 ===-->
   <?php include'footer.php';  ?>
    <!--=== End Footer Version 1 ===-->
</div>




</body>

</html>