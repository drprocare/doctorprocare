<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:14:09 GMT -->
<head>
    <title>Drprocare | For Spine</title>    

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

     <?php include 'head.php'; ?>
</head> 

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

  

    <!--=== Team v7 ===-->
    <div class="container-fluid" style="margin-top: 10px;">
        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">FOR Spine</span>

                        <span class="team-v7-name">Downward Facing -Stretch Your Hamstrings</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                       <p>This classic yoga pose is a great total body stretch that targets back extensors: the large muscles that help form your lower back, support your spine, and help you stand and lift objects.</p>

                        <p>Steps: 1. Start on your hands and knees, with your hands slightly in front of your shoulders. 2. Pressing back, raise your knees away from the floor and lift your tailbone up toward the ceiling. 3. For an added hamstring stretch, gently push your heels toward the floor.
4. Hold the position for 5 to 10 breaths, and repeat the pose five to seven times.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_spine _health/spine-1.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Child's Pose:</span>                        
                        <!-- <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p>It may look like you’re resting, but Child’s pose is an active stretch that helps elongate the back. It’s also a great de-stressor before bed at the end of a long, exhausting day.</p>

                        <p>Steps: 1. Start on all fours with your arms stretched out straight in front of you, then sit back so your glutes (butt muscles) come to rest just above — but not touching — your heels. 2. Hold the position for 5 to 10 breaths, and repeat as many times as needed for a good, soothing stretch.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_spine _health/spine-2.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Pigeon Pose:</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Pigeon pose, which can be a little challenging for yoga newbies, stretches hip rotators and flexors. It may not seem like the most obvious position to treat a backache, but tight hips can contribute to lower back pain.</p>

                        <p>Steps: 1. Start in Downward-Facing Dog with your feet together. 2. Then draw your left knee forward and turn it out to the left so your left leg is bent and near perpendicular to your right one; lower both legs to the ground. 3. You can simply keep your back right leg extended straight behind you, or for an added hamstring stretch — seasoned Pigeon posers, only! — carefully pull your back foot off the ground and in toward your back.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_spine _health/spine-3.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Triangle Pose Lengthens Torso Muscles to Build Strength</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>Triangle pose is great for strengthening the back and legs and can help lengthen your muscles along the sides of your torso while stretching the muscle fibers along your outer hip (your IT, or iliotibial, band).</p>

                        <p>Steps: 1. Start standing straight with your feet together. 2. Next, lunge your left foot back three to four feet, and point your left foot out at a 45-degree angle. Turn your chest to the side and open up the pose by stretching your right arm toward the ground and the left arm toward the ceiling, keeping both your right and left legs straight.3. You may not be able to touch the ground with your right arm at first, so don’t overstretch — only bend as far as you can while maintaining a straight back. 4. Hold the position for 5 to 10 breaths, then switch to the other side, and repeat as needed.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_spine _health/spine-4.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Cat and Cow Pose:</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p>The perfect poses for an achy, sore back, Cow and Cat stretches loosen your back muscles, whether as part of a yoga routine or as a warm-up for another workout.</p>

                        <p>Steps: 1. Starting in an all-fours position, move into Cat pose by slowly pressing your spine up and arching your back.2. Hold for a few seconds and then move to Cow by scooping your spine in, pressing your shoulder blades back and lifting your head. 3. Moving back and forth from Cat to Cow helps move your spine onto a neutral position, relaxing the muscles and easing tension. 4. Repeat 10 times, flowing smoothly from Cat into Cow, and Cow back into Cat. Repeat the sequence as needed.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_spine _health/spine-5.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">An Upward Forward Bend:</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>Sometimes called a forward fold, the upward forward bend stretches the hamstrings and back muscles while providing a release for tight, tense shoulders.</p>

                        <p>Steps: 1. Stand straight with feet shoulder-width apart and your knees loose, not locked.2. While you exhale, hinge at your waist and bend forward, reaching toward the floor. 3. Repeat the pose five to seven times. On the last bend hold the position for 5 to 10 breaths.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_spine _health/spine-6.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        
    </div>
    <!--=== End Team v7 ===-->

    

    <!--=== Footer Version 1 ===-->
   <?php include'footer.php';  ?>
    <!--=== End Footer Version 1 ===-->
</div>




</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:15:23 GMT -->
</html>