<!DOCTYPE html> 
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/shortcode_timeline1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:15:33 GMT -->
<head>
    <title>Drprocare | Diet Veg</title> 

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">

    <!-- CSS Page Style -->    
    <link rel="stylesheet" href="assets/css/pages/shortcode_timeline1.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
    <link rel="stylesheet" href="assets/css/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <style type="text/css">
        .more {display: none;}
    </style>

    <?php include 'head.php'; ?>
</head>

<body>

<div class="wrapper">
    <!--=== Header ===-->    
   <?php include 'header.php'; ?>
    <!--=== End Header ===-->

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Diet Plan for Vegetarian</h1>
            
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">     
        <div class="row">
           

            <!-- Begin Content -->
            <div class="col-md-9">
                <ul class="timeline-v1">
                    <li>
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <div class="carousel-inner">
                                        <div class="item active">
                                <img class="img-responsive" src="assets/img/diet/veg 1.jpg" alt=""/>

                                <div class="carousel-caption">
                                                <p><h2 style="background-color: white;">Day 1</h2></p>
                                            </div>
                                        </div>
                            
                            <div class="timeline-body text-justify">
                                <p><b>How to Meal Prep Your Week of Meals</b>:<br>1. Make the spiced chickpea nuts ahead of time. Cover and store at room temperature.<br>2. Prep your salad ingredients for this week. Wash your greens, shred the carrots, halve thetomatoes and slice the cucumber. Keep the salad greens separate from the chopped veggies toprevent wilting.</p>
                                <h2><a href="#">Breakfast (322 calories)</a></h2>
                                <p>Oatmeal with Fruit & Nuts</p>
                                <p>1/2 cup oatmeal cooked in 1/2 cup skim milk and 1/2 cup water</p>
                                <span id="dots1">...</span><span class="more" id="more1">
                                <p>1/2 medium apple, diced</p>
                                <p>1 Tbsp. chopped walnuts</p>
                                <p>Top oatmeal with apple, walnuts and a pinch of cinnamon.</p>
                                    <p><b><h2>A.M. Snack (47 calories)</h2></b></p>
                                    <p>1/2 medium apple</p>
                                    <p><b><h2>Lunch (337 calories)</h2></b></p>
                                    <p>Green Salad with spiced chickpea nuts</p>
                                    <p>2 cups mixed greens</p>
                                    <p>5 cherry tomatoes, halved</p>
                                    <p>1/2 cup cucumber slices</p>
                                    <p>1/4 cup spiced chickpea nuts</p>
                                    <p>1 Tbsp. feta cheese</p>
                                    <p>Combine ingredients and top with 1 Tbsp. each olive oil & balsamic vinegar.</p>
                                    <p><b><h2>P.M. Snack (80 calories)</h2></b></p>
                                    <p>1/2 cup nonfat plain Greek yogurt</p>
                                    <p>1/4 cup sliced strawberries</p>  
                                    <p><b><h2></h2></b></p>
                                    <p>Dinner (431 calories)</p>
                                    <p>1 serving Mozzarella, Basil & Zucchini Frittata</p> 
                                    <p>1 cup mixed greens topped with 1/2 Tbsp. each olive oil & balsamic vinegar.</p>
                                    <p>2 diagonal slices baguette (1/4 inch thick), preferably whole-wheat, toasted</p>
                                    <p><b>Daily Totals: 1,217 calories, 53 g protein, 126 g carbohydrates, 21 g fiber, 58 g fat, 1,183 mg sodium.</b></p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick="myFunction(1)" id="myBtn1" class="btn-u btn-u-sm">Read more</button>
                            </div>
                            </div>
                        </div>
                         
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record invert"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <div class="carousel-inner">
                                        <div class="item active">
                                <img class="img-responsive" src="assets/img/diet/veg 2.jpg" alt=""/>
                                <div class="carousel-caption">
                                                <p><h2 style="background-color: white;">Day 2</h2></p>
                                            </div>
                            </div>
                            <div class="timeline-body text-justify">
                                <p><b>Shopping Tip</b>: When buying a premade muesli, look for one without added sugars, which take away from the healthy goodness of this whole-grain breakfast.</p>
                                <h2><a href="#">Breakfast (264 calories)</a></h2>
                                <p>1 cup nonfat plain Greek yogurt</p>
                                <p>1/4 cup muesli (Muesli is a fiber- and protein-rich breakfast and brunch option based on raw, rolled oats and other diverse breakfast ingredients.)</p>
                                <p>1/4 cup blueberries</p>
                                <span id="dots2">...</span><span class="more"  id="more2">
                                    <p><b><h2>A.M. Snack (70 calories) </h2></b></p>
                                    <p>2 clementines (A clementine is a tangor, a hybrid between a willowleaf mandarin orange and a sweet orange, so named in 1902.)</p>
                                    <p><b><h2>Lunch (315 calories)</h2></b></p>
                                    <p>2 Tomato-Cheddar Cheese Toasts</p>
                                    <p>2 cups mixed greens</p>
                                    <p>1/2 cup cucumber slices</p>
                                    <p>1/4 cup grated carrot</p>
                                    <p>1 Tbsp. chopped walnuts</p>
                                 
                                    <p>Combine ingredients and top salad with 1/2 Tbsp. each olive oil & balsamic vinegar.</p>
                                    <p><b><h2>P.M. Snack (78 calories)</h2></b></p>
                                    <p>6 walnut halves</p>
                                    <p><b><h2>Dinner (422 calories)</h2></b></p>
                                    <p>2 Butternut Squash & Black Bean Tostadas</p> 
                                    <p><b><h2>Evening Snack (50 calories)</h2></b></p>
                                    <p>1 Tbsp. chocolate chips, preferably dark chocolate</p>
                                    <p><b>Meal-Prep Tip: Make a hard-boiled egg tonight to have for a snack on Day 3.</b></p>
                                    <p><b>Daily Totals: 1,199 calories, 56 g protein, 139 g carbohydrates, 25 g fiber, 56 g fat, 1,085 mg sodium.</b></p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick="myFunction(2)" id="myBtn2" class="btn-u btn-u-sm">Read more</button>
                            </div>
                        </div>
                            
                        
                    </li>
                    <li>
                        <div class="timeline-badge primarya"><i class="glyphicon glyphicon-record"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <div class="carousel slide carousel-v1" id="myCarousel">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img class="img-responsive" src="assets/img/diet/veg 3.jpg" alt=""/>
                                            <div class="carousel-caption">
                                                <p><h2 style="color: white;">Day 3</h2></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline-body text-justify">
                                
                                <h2><a href="#">Breakfast (266 calories)</a></h2>
                                <p>1 serving Peanut Butter-Banana Cinnamon Toast</p>
                              
                                <span id="dots3">...</span><span class="more" id="more3">
                                    <p><b><h2>A.M. Snack (78 calories) </h2></b></p>
                                    <p>1 hard-boiled egg seasoned with a pinch each of salt and pepper</p>
                                    <p><b><h2>Lunch (337 calories)</h2></b></p>
                                    <p>Green salad with Spiced Chickpea Nuts</p>
                                    <p>2 cups mixed greens</p>
                                    <p>1/2 cup cucumber slices</p>
                                    <p>1/4 cup Spiced Chickpea "Nuts"</p>
                                    <p>1 Tbsp. feta cheese</p>
                                 
                                    <p>Combine ingredients and top salad with 1 Tbsp. each olive oil & balsamic vinegar.</p>
                                    <p><b><h2>P.M. Snack (103 calories)</h2></b></p>
                                    <p>2/3 cup nonfat plain Greek yogurt</p>
                                    <p>3 Tbsp. blueberries</p>
                                    <p><b><h2>Dinner (426 calories)</h2></b></p>
                                    <p>1 3/4 cups Tomato & Artichoke Gnocchi</p> 
                                    <p><b><h2>Evening Snack (50 calories)</h2></b></p>
                                    <p>1 Tbsp. chocolate chips, preferably dark chocolate</p>
                                    <p><b>Meal-Prep Tip</b>: Save 1 cup of the Tomato & Artichoke Gnocchi to have for lunch on Day 4.</p>
                                    <p><b>Daily Totals: 1,199 calories, 56 g protein, 139 g carbohydrates, 25 g fiber, 56 g fat, 1,085 mg sodium.Daily Totals: 1,210 calories, 50 g protein, 149 g carbohydrates, 23 g fiber, 47 g fat, 1,482 mg sodium.</b></p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(3)" id="myBtn3" class="btn-u btn-u-sm">Read more</button>
                            </div>
                            
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <img class="img-responsive" src="assets/img/diet/veg 4.jpg" alt=""/>

                            </div>
                            <div class="timeline-body text-justify">
                                <h2><a href="#">Breakfast (264 calories)</a></h2>
                                <p>1 cup nonfat plain Greek yogurt</p>
                                <p>1/2 cup muesli</p>
                                <p>1/2 cup blueberries</p>
                                <p>Top yogurt with blueberries and muesli.</p>
                                <span id="dots4">...</span><span class="more" id="more4">
                                    <p><b><h2>A.M. Snack (105 calories) </h2></b></p>
                                    <p>8 walnut halves</p>
                                    <p><b><h2>Lunch (331 calories)</h2></b></p>
                                    <p>1 cup leftover Tomato & Artichoke Gnocchi</p>
                                    <p>2 cups mixed greens topped with 1/2 Tbsp. each olive oil & balsamic vinegar.</p>
                                   
                                    <p><b><h2>P.M. Snack (70 calories)</h2></b></p>
                                    <p>2 clementines</p>
                                    <p><b><h2>Dinner (435 calories)</h2></b></p>
                                    <p>2 1/2 cups Bean & Veggie Taco Bowl</p>
                                    <p><b>Daily Totals</b>: 1,204 calories, 56 g protein, 159 g carbohydrates, 26 g fiber, 44 g fat, 1,047 mg sodium.</p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(4)" id="myBtn4" class="btn-u btn-u-sm">Read more</button>
                            </div>
                           
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record invert"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <img class="img-responsive" src="assets/img/diet/veg 5.jpg" alt=""/>
                            </div>
                            <div class="timeline-body text-justify">
                                <h2><a href="#">Breakfast (271 calories)</a></h2>
                                <p>1 serving Avocado-Egg Toast</p>
                                
                                <span id="dots5">...</span><span class="more" id="more5">
                                    <p><b><h2>A.M. Snack (64 calories) </h2></b></p>
                                    <p>1/2 green bell pepper, sliced</p>
                                    <p>2 Tbsp. hummus</p>
                                    <p><b><h2>Lunch (354 calories)</h2></b></p>
                                    <p>1 serving Apple & Cheddar Pita Pockets</p>
                                  
                                   
                                    <p><b><h2>P.M. Snack (65 calories)</h2></b></p>
                                    <p>5 walnut halves</p>
                                    <p><b><h2>Dinner (464 calories)</h2></b></p>
                                    <p>1 2/3 cups Vegetarian Tikka Masala</p>
                                    <p>1/2 cup brown rice</p>
                                    <p>2 cups spinach, steamed</p>
                                    <p>1/2 whole-wheat pita round (6-1/2-inch)</p>
                                    <p>Meal-Prep Tip: Save 1 2/3 cup Vegetarian Tikka Masala to have for lunch on Day 6.</p>
                                    <p>Daily Totals: 1,218 calories, 55 g protein, 141 g carbohydrates, 26 g fiber, 53 g fat, 1,852 mg sodium.</p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(5)" id="myBtn5" class="btn-u btn-u-sm">Read more</button>
                            </div>
                            
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record invert"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                               <img class="img-responsive" src="assets/img/diet/veg 6.jpg" alt=""/>                 
                            </div>
                            <div class="timeline-body text-justify">
                                 <h2><a href="#">Breakfast (264 calories)</a></h2>
                                <p>1 cup nonfat plain Greek yogurt</p>
                                <p>1/2 cup muesli</p>
                                <p>1/2 cup blueberries or other berries</p>
                                <p>Top yogurt with berries and muesli.</p>
                                
                                <span id="dots6">...</span><span class="more" id="more6">
                                    <p><b><h2>A.M. Snack (60 calories) </h2></b></p>
                                    <p>1/2 cup cucumber slices</p>
                                    <p>2 Tbsp. hummus</p>
                                    <p><b><h2>Lunch (340 calories)</h2></b></p>
                                    <p>1 2/3 cups leftovers Vegetarian Tikka Masala</p>
                                    <p>1/2 whole-wheat pita round (6-1/2-inch)</p>
                                    <p>2 cups spinach, steamed</p>

                                  
                                   
                                    <p><b><h2>P.M. Snack (147 calories)</h2></b></p>
                                    <p>1 medium apple</p>
                                    <p>4 walnut halves</p>
                                    <p><b><h2>Dinner (464 calories)</h2></b></p>
                                    <p>1 serving Pita "Pizzas" with balsamic vinaigrette dressed greens</p>
                                    
                                    <p>Meal-Prep Tip: If you'll be eating lunch on-the-go on Day 7, prep your salad ingredients ahead of time.</p>
                                    <p>Daily Totals: 1,186 calories, 67 g protein, 147 g carbohydrates, 27 g fiber, 45 g fat, 1,437 mg sodium.</p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(6)" id="myBtn6" class="btn-u btn-u-sm">Read more</button>
                            </div>
                            
                    </li>

                    <li>
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record invert"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <img class="img-responsive" src="assets/img/diet/veg 7.jpg" alt=""/>
                            </div>
                            <div class="timeline-body text-justify">
                                <h2><a href="#">Breakfast (322 calories)</a></h2>
                                <p>Oatmeal with Fruit & Nuts</p>
                                <p>1/2 cup oatmeal cooked in 1/2 cup skim milk and 1/2 cup water</p>
                                <p>1/2 medium apple, diced</p>  
                                <p>1 Tbsp. chopped walnuts</p>   
                                <p>Top oatmeal with apple, walnuts and a pinch of cinnamon.</p>
                                <span id="dots7">...</span><span class="more" id="more7">
                                    <p><b><h2>A.M. Snack (47 calories) </h2></b></p>
                                    <p>1/2 medium apple</p>
                                    
                                    <p><b><h2>Lunch (315 calories)</h2></b></p>
                                    <p>2 Tomato-Cheddar Cheese Toasts</p>
                                    <p>2 cups mixed greens</p>
                                    <p>1/2 cup cucumber, sliced</p>
                                    <p>1/4 cup grated carrot</p>
                                    <p>1 Tbsp. chopped walnuts</p>
                                    <p>ombine salad ingredients and top with 1/2 Tbsp. each olive oil & balsamic vinegar.</p>
                                  
                                   
                                    <p><b><h2>P.M. Snack (42 calories)</h2></b></p>
                                    <p>1/2 cup blueberries</p>
                                    <p><b><h2>Dinner (400 calories)</h2></b></p>
                                    <p>1 1/2 cups Farmers' Market Fried Rice</p>
                                    <p><b><h2>Evening Snack (50 calories)</h2></b></p>
                                    <p>1 Tbsp. chocolate chips, preferably dark chocolate</p>
                                  
                                    <p>Daily Totals: 1,210 calories, 36 g protein, 177 g carbohydrates, 24 g fiber, 45 g fat, 855 mg sodium.</p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(7)" id="myBtn7" class="btn-u btn-u-sm">Read more</button>
                            </div>
                           
                        </div>
                    </li>
                    <li class="clearfix" style="float: none;"></li>
                </ul>
            </div>
            <!-- End Content -->
        </div>          
    </div><!--/container-->     
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
   <?php include 'footer.php'; ?>
    <!--=== End Footer Version 1 ===-->
</div><!--/End Wrapepr-->

<script src="assets/js/diet.js"></script>

</body>

</html> 