
 <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.html">     
    <link rel="stylesheet" href="assets/plugins/parallax-slider/css/parallax-slider.css">
     <link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
     

    <!-- CSS Page Style -->    
    <link rel="stylesheet" href="assets/css/pages/portfolio-v2.css">
    <link rel="stylesheet" href="assets/css/pages/page_about.css">
    <link rel="stylesheet" type="text/css" href="assets/css/blue.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/blue.css">
    <link rel="stylesheet" href="assets/css/theme-skins/dark.css">


    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

     <link rel="shortcut icon" href="favicon.icon">