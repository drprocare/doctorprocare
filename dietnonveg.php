<!DOCTYPE html> 
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/shortcode_timeline1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:15:33 GMT -->
<head>
   <title>Drprocare | Diet Non Veg</title> 

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">

    <!-- CSS Page Style -->    
    <link rel="stylesheet" href="assets/css/pages/shortcode_timeline1.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
    <link rel="stylesheet" href="assets/css/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <style type="text/css">
        .more {display: none;}
    </style>

    <?php include 'head.php'; ?>
</head>

<body>

<div class="wrapper">
    <!--=== Header ===-->    
   <?php include 'header.php'; ?>
    <!--=== End Header ===-->

    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Diet Plan for Non-Vegetarian</h1>
           
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">     
        <div class="row">
           

            <!-- Begin Content -->
            <div class="col-md-9">
                <ul class="timeline-v1">
                    <li>
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <div class="carousel-inner">
                                        <div class="item active">
                                <img class="img-responsive" src="assets/img/diet/non veg 1.jpg" alt=""/>

                                <div class="carousel-caption">
                                                <p><h2 style="background-color: white;">Day 1</h2></p>
                                            </div>
                                        </div>
                            
                            <div class="timeline-body text-justify">
                                
                                <h2><a href="#">Breakfast (approx 400 cals) </a></h2>
                                <p>1 glass Whey protein, 3 boiled eggs</p>
                                <p>Morning Snack (approx 200 cals) </p>
                                <span id="dots1">...</span><span class="more" id="more1">
                                <p>100g smoked salmon with spinach </p>
                                <p>Lunch (approx 600 cals)</p>
                                <p>100g boiled chicken, cheese and vegetable sandwich</p>
                                    <p><b><h2>Evening Snack (approx 200 calories)</h2></b></p>
                                    <p>1 bowl yogurt with fruits</p>
                                    <p><b><h2>Dinner(400 calories)</h2></b></p>
                                    <p>- 200g grilled fish, brown rice, and vegetable salad</p>
                                    <p><b><h2></h2></b></p>
                                  
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick="myFunction(1)" id="myBtn1" class="btn-u btn-u-sm">Read more</button>
                            </div>
                            </div>
                        </div>
                            
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record invert"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <div class="carousel-inner">
                                        <div class="item active">
                                <img class="img-responsive" src="assets/img/diet/non veg 2.jpg" alt=""/>
                                <div class="carousel-caption">
                                                <p><h2 style="background-color: white;">Day 2</h2></p>
                                            </div>
                            </div>
                            <div class="timeline-body text-justify">
                                <h2><a href="#">Breakfast (approx 400 cals):</a></h2>
                                <p>1 glass Whey protein, an omelet made with three eggs</p>
                                <span id="dots2">...</span><span class="more"  id="more2">
                                    <p><b><h2>A.M. Snack (200 calories) </h2></b></p>
                                    <p>100g grilled turkey breast</p>
                                    <p><b><h2>Lunch (315 calories)</h2></b></p>
                                    <p>1 Chicken roll and vegetable salad</p>
                                    <p><b><h2>P.M. Snack (200 calories)</h2></b></p>
                                    <p>Handful of almonds and walnuts</p>
                                    <p><b><h2>Dinner (4 calories)</h2></b></p>
                                    <p>2 100g grilled lamb with steamed broccoli and spinach</p> 
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick="myFunction(2)" id="myBtn2" class="btn-u btn-u-sm">Read more</button>
                            </div>
                        </div>
                           
                        
                    </li>
                    <li>
                        <div class="timeline-badge primarya"><i class="glyphicon glyphicon-record"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <div class="carousel slide carousel-v1" id="myCarousel">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img class="img-responsive" src="assets/img/diet/non veg 3.jpg" alt=""/>
                                            <div class="carousel-caption">
                                                <p><h2 style="color: white;">Day 3</h2></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline-body text-justify">
                                
                                <h2><a href="#">Breakfast (400 calories)</a></h2>
                                <p>1 glass Whey Protein, 3 boiled eggs</p>
                              
                                <span id="dots3">...</span><span class="more" id="more3">
                                    <p><b><h2>A.M. Snack (200 calories) </h2></b></p>
                                    <p>100g grilled chicken</p>
                                    <p><b><h2>Lunch (600 calories)</h2></b></p>
                                    <p>300g grilled fish, vegetable salad, dal and rice</p>
                                    <p><b><h2>P.M. Snack (200 calories)</h2></b></p>
                                    <p>1 bowl yogurt with fruits</p>
                                   
                                    <p><b><h2>Dinner (400 calories)</h2></b></p>
                                    <p>Whole wheat bread beef, cheese, and vegetable sandwich</p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(3)" id="myBtn3" class="btn-u btn-u-sm">Read more</button>
                            </div>
                            
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-badge primarya"><i class="glyphicon glyphicon-record"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <div class="carousel slide carousel-v1" id="myCarousel">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img class="img-responsive" src="assets/img/diet/non veg 4.jpg" alt=""/>
                                            <div class="carousel-caption">
                                                <p><h2 style="color: white;">Day 4</h2></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline-body text-justify">
                                <h2><a href="#">Breakfast (400 calories)</a></h2>
                                <p>glass Whey protein, 3 boiled eggs</p>
                                <span id="dots4">...</span><span class="more" id="more4">
                                    <p><b><h2>A.M. Snack (200 calories) </h2></b></p>
                                    <p>100g smoked salmon with spinach</p>
                                    <p><b><h2>Lunch (600 calories)</h2></b></p>
                                    <p>100g boiled chicken, 2 Roti, 1 cup rice with dal</p>
                                   
                                    <p><b><h2>P.M. Snack (200 calories)</h2></b></p>
                                    <p>1 glass whey and banana shake</p>
                                    <p><b><h2>Dinner (400 calories)</h2></b></p>
                                    <p>200g grilled fish, brown rice, and vegetable salad</p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(4)" id="myBtn4" class="btn-u btn-u-sm">Read more</button>
                            </div>
                            
                        </div>
                    </li>
                    <li>
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record invert"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <img class="img-responsive" src="assets/img/diet/non veg 4.jpg" alt=""/>
                            </div>
                            <div class="timeline-body text-justify">
                                <h2><a href="#">Breakfast (400 calories)</a></h2>
                                <p>1 glass Whey protein, an omelet made with three eggs</p>
                                
                                <span id="dots5">...</span><span class="more" id="more5">
                                    <p><b><h2>A.M. Snack (200 calories) </h2></b></p>
                                    <p>100g grilled turkey breast</p>
                                    <p><b><h2>Lunch (600 calories)</h2></b></p>
                                    <p>1 Chicken roll and vegetable salad</p>
                                  
                                   
                                    <p><b><h2>P.M. Snack (200 calories)</h2></b></p>
                                    <p>Handful of almonds and walnuts</p>
                                    <p><b><h2>Dinner (400 calories)</h2></b></p>
                                    <p>100g grilled lamb with steamed broccoli and spinach</p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(5)" id="myBtn5" class="btn-u btn-u-sm">Read more</button>
                            </div>
                            
                        </div>
                    </li>
                    <li class="timeline-inverted">
                         <div class="timeline-badge primarya"><i class="glyphicon glyphicon-record"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <div class="carousel slide carousel-v1" id="myCarousel">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img class="img-responsive" src="assets/img/diet/non veg 5.jpg" alt=""/>
                                            <div class="carousel-caption">
                                                <p><h2 style="color: white;">Day 5</h2></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline-body text-justify">
                                 <h2><a href="#">Breakfast (400 calories)</a></h2>
                                <p>1 glass Whey Protein, 3 boiled eggs</p>
                                
                                <span id="dots6">...</span><span class="more" id="more6">
                                    <p><b><h2>A.M. Snack (200 calories) </h2></b></p>
                                    <p>100g grilled chicken</p>
                                    <p><b><h2>Lunch (600 calories)</h2></b></p>
                                    <p>300g grilled fish, vegetable salad, dal and rice</p>
                                   
                                    <p><b><h2>P.M. Snack (200 calories)</h2></b></p>
                                    <p>1 bowl yogurt with fruits</p>
                                    <p><b><h2>Dinner (400 calories)</h2></b></p>
                                    <p>Whole wheat bread beef, cheese, and vegetable sandwich</p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(6)" id="myBtn6" class="btn-u btn-u-sm">Read more</button>
                            </div>
                           
                    </li>

                    <li>
                        <div class="timeline-badge primary"><i class="glyphicon glyphicon-record invert"></i></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <img class="img-responsive" src="assets/img/diet/non veg 6.jpg" alt=""/>
                            </div>
                            <div class="timeline-body text-justify">
                                <h2><a href="#">Breakfast (400 calories)</a></h2>
                                <p>1 glass Whey protein, an omelet made with three eggs</p>
                                <span id="dots7">...</span><span class="more" id="more7">
                                    <p><b><h2>A.M. Snack (200 calories) </h2></b></p>
                                    <p>100g grilled turkey breasts</p>
                                    <p><b><h2>Lunch (200 calories)</h2></b></p>
                                    <p>100g boiled chicken, cheese and vegetable sandwich</p>
                                    <p><b><h2>P.M. Snack (42 calories)</h2></b></p>
                                    <p>1 glass whey and banana shake</p>
                                    <p><b><h2>Dinner (400 calories)</h2></b></p>
                                    <p>200g grilled fish, brown rice, and vegetable salad</p>
                                </span></p>
                                <!-- <a class="btn-u btn-u-sm" href="#">Read More</a> -->
                                <button onclick=" myFunction(7)" id="myBtn7" class="btn-u btn-u-sm">Read more</button>
                            </div>
                            
                        </div>
                    </li>
                    <li class="clearfix" style="float: none;"></li>
                </ul>
            </div>
            <!-- End Content -->
        </div>          
    </div><!--/container-->     
    <!--=== End Content Part ===-->

     <!--=== Footer Version 1 ===-->
   <?php include 'footer.php'; ?>
    <!--=== End Footer Version 1 ===-->
</div><!--/End Wrapepr-->

<script src="assets/js/diet.js"></script>

</body>

</html> 