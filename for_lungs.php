<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:14:09 GMT -->
<head>
    <title>Drprocare | For Lungs</title>    

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="assets/plugins/animate.css">
    <link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="assets/css/custom.css">

     <?php include 'head.php'; ?>
</head> 

<body>

<div class="wrapper">
    <!--=== Header ===-->    
    <?php include 'header.php'; ?>
    <!--=== End Header ===-->

  

    <!--=== Team v7 ===-->
    <div class="container-fluid" style="margin-top: 10px;">
        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">For Lungs</span>
                        <span class="team-v7-name">Shavasana</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>Shavasana pose provides asthma relief because of the breath and stress management.</p>
                         <p>How to do it: 1. Lie on your back with your arms at your sides and your feet and palms dropped open. 2. Close your eyes and soften your jaw, taking your focus inward. 3. Start to focus your attention on your breath and slow it down, making it deep and rhythmic, relaxing every part of your body. 4. Stay in the pose for 5 to 10 minutes, maintaining slow, even breathing.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_lungs/lungs-1.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Sukasana</span>                        
                        <!-- <span class="team-v7-position">Co-Founder/ UX Design</span> -->
                        <p>The sukasana pose is another relaxing pose for natural asthama relief. Just like savasana, its focus on breath and stress control makes it a great exercise to help asthma and lung function in particular.</p>

                        <p>How to do it:
1. Start seated, with your legs crossed.
2. If you feel some discomfort in your hips or lower back, roll up a towel and place it under your sitz bone (tailbone) for extra support.
3. Take your right hand and place it on your heart, place the left hand on your belly, and close your eyes.
4. Draw in the stomach and lift the chest for good posture.
5. Let out your breath slowly and hold the pose for 5 minutes, with slow, even breathing.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_lungs/lungs-2.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Forward Bend Asana</span>
                       <!--  <span class="team-v7-position">Co-Founder / CEO</span> -->
                        <p>This bending pose can open up the lungs for natural asthma relief.</p>

                        <p>How to do it:
1. Stand with your legs hip-width apart, fold your body forward.
2. Put a little bend in the knees to relieve any strain in the lower back.
3. Fold your arms, holding each elbow with the opposite hand, and let your body hang as you take five deep breaths with your eyes closed.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_lungs/lungs-3.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Butterfly Pose</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>The butterfly is a popular relaxing that can yoga pose bring asthma relief, which makes it another top stretching exercise to help with asthma.</p>

                        <p>How to do it:
1. Sit with the soles of your feet together and your knees dropped out to the sides — picture your legs as two butterfly wings.
2. Hold your ankles and pull your heels into your hips.
3. Inhale deeply and, as you exhale, fold your body forward.
4. You can use your elbows to press down your knees a bit more.
5. Hold this pose as you take five deep breaths.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_lungs/lungs-4.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Straddled Splits</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p>This yoga pose for asthma relief involves stretching out the upper body and opening up the lungs.</p>

                        <p>How to do it:
1. Sit with your legs straddled wide apart and your heels flexed.
2. Firm the thighs, and follow with a big inhale as you reach your arms up.
3. As you exhale, fold your body forward, walking your hands out in front of you.
4. Hold this pose as you take five deep breaths.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_lungs/lungs-5.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

         <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 col-md-push-6 team-arrow-left">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Bridge pose</span>                        
                        <span class="team-v7-position">Co-Founder/ UX Design</span>
                        <p>How to do it:
1. To begin, lie flat on your back with your arms parallel with your body.
2. Bend your knees and slide your feet toward your butt until your feet are directly under your knees; then lift your butt up off the floor.
3. Think of lifting your tailbone upward toward the ceiling and bringing your thighs parallel with the floor.
4. At the same time, your shoulder blades should be pressed in toward your back, expanding your chest.
5. Hold for 30 seconds to 1 minute, and exhale as you gently lower your spine to the ground.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_lungs/lungs-6.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

        <!-- Team Blcoks -->
        <div class="row team-v7 no-gutter equal-height-columns">
            <div class="col-md-6 team-arrow-right">
                <div class="dp-table">
                    <div class="equal-height-column dp-table-cell team-v7-in" style="height: 555px;">
                        <span class="team-v7-name">Supportive Fish Aasan:</span>
                        <span class="team-v7-position">Co-Founder / CEO</span>
                        <p>The funny-sounding "supportive fish" is another exercise to help asthma. This pose is good for the health of your bronchial tube.</p>

                        <p>How to do it:
1. Start by placing a thin, rolled-up towel under your mid-back and a thicker rolled-up towel behind your neck.
2. When the towels are in place and you're on your back, the bridge of your nose should be aligned with your chin.
3. Extend your legs straight out, relax your feet, and rest your arms at a 45-degree angle from your hips, with palms upward.
4. Simply relax and breathe in this position.</p>
                        <ul class="list-inline social-icons-v1">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 team-v7-img">
                <img class="img-responsive full-width equal-height-column" src="assets/img/yoga/for_lungs/lungs-7.jpg" alt="" style="height: 555px;">
            </div>
        </div>
        <!-- End Team Blcoks -->

       
    </div>
    <!--=== End Team v7 ===-->

    

    <!--=== Footer Version 1 ===-->
   <?php include'footer.php';  ?>
    <!--=== End Footer Version 1 ===-->
</div>




</body>

<!-- Mirrored from htmlstream.com/preview/unify-v1.8/feature_team_blocks.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 28 Jan 2016 17:15:23 GMT -->
</html>